# SETUP microk8s
This is a script to setup a microk8s using snap on an newly installed ubuntu machine.
This script is designed to use UBUNTU LTS only

By using install.sh the setup - script will be installed in **/opt/local/setup/**
The last step of **install.sh** ist to create an setup user
```
...

## CREATING USER SETUP
useradd  -s /opt/local/setup/setup.sh -m -ou 0 -g 0  -c "1ST RUN WIZZARD USER" setup
usermod -aG root setup

## GIVE IT A PASSWORD
echo -n "ENTER PASSWORD FOR SETUP USER: "
read -s PWD
echo -e "${PWD}\n${PWD}\n" | passwd setup

...

```

## Getting started

### EDIT CONFIG FILE
First of all you have to modify the setup.cfg to your needs.
You will find this file in **/opt/local/setup/config/setup.cfg**
```
#!/bin/bash

##########################################
## MICROK8S
##########################################
DOCKER_REG_HOSTS="registry.example.domain"
DOCKER_REG_PORT="443"
DOCKER_REG_USR="YOUR_REGISTRY_USER"
DOCKER_REG_PWD="YOUR_REGISTRY_PASSWORD"
CONTAINERD_TEMPLATE="/var/snap/microk8s/current/args/containerd-template.toml"
MK8S_STORAGE="/var/snap/microk8s/common/default-storage"
MK8S_CERTS="/var/snap/microk8s/current/args/certs.d"
MK8S_CHANNEL="1.28/stable"

##########################################
## GLOBAL SETTINGS
##########################################
COUNT="0"
TIMEOUT="300"
```
Modify the docker params to fit your local and private registry.
Change MK8S_CHANNEL to the latest stable. For the moment **1.28** is OK.
Keep MK8S_STORAGE and MK8S_CERTS untouched.
Copy the IssuingCA which singned your local and private registry https:// - Cert in **/opt/local/setup/ssl/IssuingCA.crt**
Important :: CA must be named **IssuingCA.crt** 

Create a lvm and mount it to /data. All your container stuff will be stored there from now on.

## START SETUP

Log into you system with user **setup** and the password you set.
Or run **/opt/local/scripts/setup/setup.sh**
Or simply run **su - setup** if you are already loged in.

## You will now reach a menue

```
                                                                               
                                MICROK8S SETUP SCRIPT                          
                                                                               

                                       :::                                                  
                                   :=*******=:                                              
                               :=+*************+=                                           
                            *************************                                       
                        *+*****************************+*                                   
                    :+*************************************+:                               
                :+********************+   *********************=:                           
            :=************************+   *************************=:                       
          :****************************  :****************************:                     
          *****************************  :*****************************:                    
         =************************+*         *+*************************                    
         *********=:*+********=:                 :=********+::=*********                    
        =**********   :****+:        ::   ::        :+****:   ***********                   
        +***********=   :*:     *+*****   :****+:     :*:   =***********+                   
       :**************+       +*******:   :*******=       +**************:                  
       =***************:      :+******:   :*******:      :***************=                  
       ***************:          =****:    +***=          :***************                  
      =***************   :***      :+*     *+:      =**    ****************                 
      +*************+    =*****                   =****+    +**************                 
     ***************=    *******=               =******+:   =***************                
     +***************   *********:    =***=    :+********   ***************+                
     ****************   =**+*         +***+         *+***   ***************+:               
    =****************                 :***:                 ****************=               
    **********+=*:              :*            :*:              :*=+**********               
   **********:    :*:    :+*******+           +*********    :*:    :**********              
   =**********++*****+    =*******     :+:    :*******=    ******++**********+              
   *******************+    *******    *****    *******    =******************:              
     ******************+     =**=    *******    =**+     +*****************+:               
      =******************:     *    *********    =     :******************=                 
       :******************+:       :+********:       :+******************:                  
         +********************                     ********************=                    
          :*******************   :             :   *******************:                     
            +****************:  #******+++******+  :****************=                       
             **************+:  +*****************+   +*************:                        
               +************  *********************  =***********+                          
                ************++*********************++************                           
                  +*******************************************+                             
                   *******************************************                              
                    :+**************************************                                
                      *************************************                                 
                         ******************************:
                                                                       
                                                                               
                                                                               

  PLEASE MAKE SURE YOU HAVE FULLY SETUP YOUR HOST BY USING [ SETUP HOST ] 
  BEFORE INITIATING MICROK8S INSTALLATION!!!
  CHECK TWICE!!!


--------------------------------------------------------------
 [ 0 ] :: SETUP HOST
 [ 1 ] :: SETUP MICROK8S
 [ 3 ] :: CLEANUP SYSTEM
 ---------------------
 [ 10 ] :: POWEROFF
 [ 11 ] :: REBOOT
--------------------------------------------------------------

YOUR CHOICE: 

```
First of all, you have to setup your host config, because of microk8s is binding to that config.
If you don't do so, and you change your host settings after initiatin microk8s installation, microk8s will not work any more.

If you have made mistakes you'll be able to remove microk8s by executing
```
snap remove microk8s
```
Start the setup process again by step **[ 1 ]**

## profiles.d
To make usage simple a file called **kubernetes.sh** is stored in **/etc/profile.d/**
This file includes som aliases see below...
```
alias kubectl='microk8s.kubectl'

source <(microk8s.kubectl completion bash)
alias allnodes='microk8s.kubectl get nodes -o wide'
alias allpods='microk8s.kubectl get pods -o wide --all-namespaces | grep -v kube-system'
alias syspods='microk8s.kubectl get pods -o wide -n kube-system'
alias pvcs='microk8s.kubectl get pvc -o wide --all-namespaces'

alias container='cd /opt/config/container/; ls -l' 
```

As in vanilla kubernetes you'll now be able to use the same commands with microk8s
**kubectl** is an **alias** to **microk8s.kubectl**

Hope it will help you palying around with kubernetes.
