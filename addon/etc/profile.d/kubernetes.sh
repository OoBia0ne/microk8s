alias kubectl='microk8s.kubectl'

source <(microk8s.kubectl completion bash)
alias allnodes='microk8s.kubectl get nodes -o wide'
alias allpods='microk8s.kubectl get pods -o wide --all-namespaces | grep -v kube-system'
alias syspods='microk8s.kubectl get pods -o wide -n kube-system'
alias pvcs='microk8s.kubectl get pvc -o wide --all-namespaces'

alias container='cd /opt/config/container/; ls -l' 

