#!/bin/bash
########################################################################################
##
##      WRITTEN FOR Gentoo LINUX
##
##      VERSION:        0.0.1
##      DATE:           01.03.2004
##      MAINTAINER:     mad.murdock
##      LICENZE:        GPLv2
##
########################################################################################

# ANSI COLORS
NORMAL='\e[0;39m'
RED='\e[1;31m'
GREEN='\e[1;32m'
YELLOW='\e[1;33m'
BLUE='\e[1;34m'
MAGENTA='\e[1;35m'
CYAN='\e[1;36m'
WHITE='\e[1;37m'
BLACK='\e[30;40m'
GREY='\e[90m'
BG_GREY='\e[1;100m'
BG_DEFAULT='\e[1;49m'
BG_RED='\e[1;41m'
BLINK='\e[1;5m'
BLACK='\e[1;30m'
