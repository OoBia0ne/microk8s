#!/bin/bash

##########################################
## GETTING CURRENT NETWORK INFOS FROM FILE
## UBUNTU ONLY
##########################################
function get_network_info () {

	DOMAIN_NAME=$( hostname -d )
	if [ -f /etc/netplan/01-netcfg.yaml ]; then
		NETWORK_DEVICE=$( grep -A1 ethernets /etc/netplan/01-netcfg.yaml | tail -1 | sed 's/^\ *//g;s/\://g' )
		HOST_IP=$( grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\/[0-9]{1,2}\b" /etc/netplan/01-netcfg.yaml | head -1 )
		GATE_WAY=$( grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" /etc/netplan/01-netcfg.yaml | head -2 | tail -1 )
		DNS_01=$( grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" /etc/netplan/01-netcfg.yaml | tail -1 )
	fi

	#########################################
	## SETTING DEFAULT VALUES IF VAR IS EMPTY
	#########################################
        : "${NETWORK_DEVICE:=eth0}"
        : "${DOMAIN_NAME:=example.domain}"
        : "${HOST_IP:=10.0.0.251/24}"
        : "${GATE_WAY:=10.0.0.1}"
        : "${DNS_01:=10.0.10.11}"
        : "${DNS_02:=10.0.10.12}"
}

