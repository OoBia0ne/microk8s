#!/bin/bash
########################################################################################
##
##      WRITTEN FOR UBUNTU
##
##      VERSION:        0.1.1
##      DATE:           24.12.2023
##      MAINTAINER:     mad.murdock
##      LICENZE:        GPLv2
##
########################################################################################
########################################################
## SETTING UP MICROK8S
########################################################
function install_microk8s () {
	apt list --installed | grep snap
	if [ $? -ne 0 ]; then
		sudo apt install -y snapd pigz
	fi

	snap list microk8s &>/dev/null
	if [ $? -ne 0 ]; then
		msg_ok "INSTALLING" "microk8s --classic --channel=${MK8S_CHANNEL}"
		cleanup
		snap install microk8s --classic --channel=${MK8S_CHANNEL}
		apt install pigz
	else 
		## microk8s.reset
		microk8s.kubectl delete $( microk8s.kubectl get node -o name )
		service snap.microk8s.daemon-kubelite restart
		
		microk8s.kubectl create namespace public

		for DOCKER_REG in ${DOCKER_REG_HOSTS} 
		do
			DOCKER_REG_HOST="$( echo ${DOCKER_REG} | awk -F ';' '{ print $1 }' )"
			if [ ! "${DOCKER_REG_PORT}" ]; then
				DOCKER_SERVER+="--docker-server=${DOCKER_REG_HOST} "
			else
				DOCKER_SERVER+="--docker-server=${DOCKER_REG_HOST}:${DOCKER_REG_PORT} "
			fi
		done

		sleep 5

	fi

	msg_ok "ENABLING REPOSITORY" "COMMUNITY"
	microk8s.enable community

	msg_ok "ENABLING" "hostpath-storage"
	microk8s.enable hostpath-storage

	msg_ok "ENABLING" "DNS"
	microk8s.enable dns 

	msg_ok "CREATING SYMLINK TO LOCAL STORAGE" "${MK8S_STORAGE} ==> /data"
	if [ ! -L "${MK8S_STORAGE}" ]; then
		rm -rf ${MK8S_STORAGE}
		mkdir -p /data
		ln -sf /data ${MK8S_STORAGE}
	fi

	###################################################
	## ENABLING PRIVATE DOCKER REGISTRY
	###################################################
	for DOCKER_REG in ${DOCKER_REG_HOSTS}; do
		DOCKER_REG_HOST="$( echo ${DOCKER_REG} | awk -F ';' '{ print $1 }' )"

		############################
		## CREATING HOSTS FILE
		############################
		DOCKER_REG_FOLDER=${MK8S_CERTS}/${DOCKER_REG_HOST}
		mkdir -p ${DOCKER_REG_FOLDER}/ 2>/dev/null

		echo  "server = \"https://${DOCKER_REG_HOST}\""  >  ${DOCKER_REG_FOLDER}/hosts.toml 
		echo  "[host.\"https://${DOCKER_REG_HOST}\"]"    >> ${DOCKER_REG_FOLDER}/hosts.toml
		echo  "  capabilities = [\"pull\", \"resolve\"]" >> ${DOCKER_REG_FOLDER}/hosts.toml
		echo  "  ca = \"${DOCKER_REG_FOLDER}/IssuingCA.crt\"" >> ${DOCKER_REG_FOLDER}/hosts.toml
	
		cp ${MY_DIR}/ssl/IssuingCA.crt ${DOCKER_REG_FOLDER}

		############################
		## CREATING HOSTS:PORT FILE
		############################
		DOCKER_REG_FOLDER=${MK8S_CERTS}/${DOCKER_REG_HOST}\:${DOCKER_REG_PORT}
		mkdir -p ${DOCKER_REG_FOLDER}/ 2>/dev/null

		echo  "server = \"https://${DOCKER_REG_HOST}:${DOCKER_REG_PORT}\"" >  ${DOCKER_REG_FOLDER}/hosts.toml 
		echo  "[host.\"https://${DOCKER_REG_HOST}:${DOCKER_REG_PORT}\"]" >> ${DOCKER_REG_FOLDER}/hosts.toml
		echo  "  capabilities = [\"pull\", \"resolve\"]" 	 	 >> ${DOCKER_REG_FOLDER}/hosts.toml
		echo  "  ca = \"${DOCKER_REG_FOLDER}/IssuingCA.crt\"" 	 	 >> ${DOCKER_REG_FOLDER}/hosts.toml
	
		cp ${MY_DIR}/ssl/IssuingCA.crt ${DOCKER_REG_FOLDER}

		msg_ok "PRIVATE DOCKER REGISTRY EANBALED" "${DOCKER_REG_FOLDER}/hosts.toml"
	done

	msg_ok "MICROK8S INTANCE IS GOING TO BE" "RESTARTED"
	microk8s.stop

	if [ -f ${CONTAINERD_TEMPLATE} ]; then
		msg_ok "CONTAINERD IS GOING TO BE MODIFIED" "/var/snap/microk8s/current/args/containerd-template.toml"
		grep "${DOCKER_REG_PWD}" ${CONTAINERD_TEMPLATE} &>/dev/null
		if [ $? -ne 0 ]; then
			echo "    [plugins.\"io.containerd.grpc.v1.cri\".registry.configs.\"${DOCKER_REG_HOSTS}\".auth]" >> ${CONTAINERD_TEMPLATE}
			echo "      username = \"${DOCKER_REG_USR}\"" >> ${CONTAINERD_TEMPLATE}
			echo "      password = \"${DOCKER_REG_PWD}\"" >> ${CONTAINERD_TEMPLATE}
		fi
	fi	

	microk8s.start

	cp -rfp ${MY_DIR}/addon/* /
	[[ -f /etc/profile.d/kubernetes.sh ]] && . /etc/profile.d/kubernetes.sh

	microk8s.kubectl create ns public

	###################################################
	## CREATING K8S - SECRET  
	###################################################
	msg_ok "MICROK8S CREATING SECRET" "registrypass"
	microk8s.kubectl create secret docker-registry registrypass \
		--docker-username=${DOCKER_REG_USR} \
		--docker-password=${DOCKER_REG_PWD} \
		${DOCKER_SERVER} \
		-n public

	## DEBUG
	#echo "microk8s.kubectl create secret docker-registry registrypass \
	#	--docker-username=${DOCKER_REG_USR} \
	#	--docker-password=${DOCKER_REG_PWD} \
	#	${DOCKER_SERVER} \
	#	-n public"
}

########################################################
## INSTALL TRAEFIK POD
########################################################
function install_traefik () {
	
	msg_ok "TRAEFFIC IS GOING TO BE" "INSTALLED"

	IP=$( grep address /etc/netplan/01-netcfg.yaml | head -1 | awk '{ print $3 }' )
	IP=${IP%%/*}

	rsync -av ${MY_DIR}/template/traefik/traefik_v3.x /var/tmp/
	find /var/tmp/traefik_v3.x -type f -exec sed -i s/nc-externalip/${IP}/g {} \;
	sleep 10
	microk8s.kubectl apply -f /var/tmp/traefik_v3.x/
	microk8s.kubectl apply -f /var/tmp/traefik_v3.x/middleware/
	sleep 5
	microk8s.kubectl apply -f /var/tmp/traefik_v3.x/
	microk8s.kubectl apply -f /var/tmp/traefik_v3.x/middleware/
}

