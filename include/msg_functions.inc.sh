########################################################################################
##
##      WRITTEN FOR Gentoo LINUX
##
##      VERSION:        0.3.2
##      DATE:           19.02.2009
##      MAINTAINER:     Mad Murdock
##      LICENZE:        GPLv2
##
########################################################################################

########################################################
## MESSAGE HANDLER
########################################################
function msg_ok () {
        MESSAGE_1="$( echo $1 | tr '[:lower:]' '[:upper:]' )"
        MESSAGE_2="$2"
                if [ ! "${MESSAGE_2}" ]; then
                	echo -e "${GREEN} >> ${WHITE}${MESSAGE_1}${NORMAL}"
                else
                	echo -e "${GREEN} >> ${WHITE}${MESSAGE_1}${NORMAL} ${GREEN}[${WHITE} ${MESSAGE_2} ${GREEN}]${NORMAL}"
                fi

                }

function msg_error () {
        MESSAGE_1="$( echo $1 | tr '[:lower:]' '[:upper:]' )"
        MESSAGE_2="$2"
                if [ ! "${MESSAGE_2}" ]; then
                	echo -e "${RED} >> ${WHITE}${MESSAGE_1}${NORMAL}"
                else
                	echo -e "${RED} >> ${WHITE}${MESSAGE_1}${NORMAL} ${RED}[${WHITE} ${MESSAGE_2} ${RED}]${NORMAL}"
                fi
        }

function msg_info () {
        MESSAGE_1="$( echo $1 | tr '[:lower:]' '[:upper:]' )"
        MESSAGE_2="$2"
        if [ -z ${BY_NAGIOS} ]; then
                echo -e "${YELLOW} >> ${WHITE}${MESSAGE_1}${NORMAL} ${YELLOW}[${WHITE} ${MESSAGE_2} ${YELLOW}]${NORMAL}"
        else
                echo -e " >> ${MESSAGE_1} [ ${MESSAGE_2} ]"
        fi
        }
function msg_help () {
        MESSAGE_1="$( echo $1 | tr '[:lower:]' '[:upper:]' )"
                BY_NAGIOS="$3"
                if [ -z ${BY_NAGIOS} ]; then
                echo -e "    ${GREY}${MESSAGE_1}${NORMAL}"
        else
                echo -e "    ${MESSAGE_1}"
                fi
        }

function msg_ask () {
        MESSAGE_1="$( echo $1 | tr '[:lower:]' '[:upper:]' )"
        MESSAGE_2="$( echo $2 | tr '[:lower:]' '[:upper:]' )"
                if [ -z ${BY_NAGIOS} ]; then
                        echo
                	echo -en "${RED} :: ${WHITE}${MESSAGE_1}${NORMAL} ${RED}[${WHITE} ${MESSAGE_2} ${RED}] ${NORMAL}"
        	else
                        echo
                	echo -e " :: ${MESSAGE_1} [ ${MESSAGE_2} ]"
                fi
        }


function msg_status () {
        MESSAGE_1="$( echo $1 | tr '[:lower:]' '[:upper:]' )"
        #MESSAGE_2="$( echo $2 | tr '[:lower:]' '[:upper:]' )"
        MESSAGE_2="$2"
                BY_NAGIOS="$3"
                if [ -z ${BY_NAGIOS} ]; then
                echo -e "${GREEN} ${WHITE}${MESSAGE_1}${NORMAL}         ${WHITE}[ ${MESSAGE_2} ${WHITE}]${NORMAL}"
        else
                echo -e " >> ${MESSAGE_1} [ ${MESSAGE_2} ]"
                fi
        }

function msg_usage () {
        MESSAGE_1="$( echo $1 | tr '[:lower:]' '[:upper:]' )"
        #MESSAGE_2="$( echo $2 | tr '[:lower:]' '[:upper:]' )"
        MESSAGE_2="$2"
                BY_NAGIOS="$3"
                if [ -z ${BY_NAGIOS} ]; then
                echo -e "${YELLOW} ${WHITE}${MESSAGE_1}${NORMAL} ${RED}[${WHITE} ${MESSAGE_2} ${RED}]${NORMAL}"
        else
                echo -e " >> ${MESSAGE_1} [ ${MESSAGE_2} ]"
                fi
        }



