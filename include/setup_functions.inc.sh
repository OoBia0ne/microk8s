#!/bin/bash
########################################################################################
##
##      WRITTEN FOR Gentoo LINUX
##
##      VERSION:        0.0.1
##      DATE:           01.03.2020
##      MAINTAINER:     mad.murdock
##      LICENZE:        GPLv2
##
########################################################################################

#########################################
## READ HOSTCONFIG
#########################################
function host_config () {
        HOST_NAME=$( cat /etc/hostname )
        if [ ! "${HOST_NAME}" ]; then
                HOST_NAME=$( hostname -f )
                if [ ! "${HOST_NAME}" ]; then
                        HOST_NAME="${HOSTNAME}"
                fi
        fi
}

#########################################
## CREATE INTERFACE CONFIG 
## UBUNTU >= 18.04 ONLY
#########################################
function create_netplan_cfg () {
	if [ ! "${DNS02}" ]; then
cat >/etc/netplan/01-netcfg.yaml<<EOF
## 1ST RUN WIZZARD
network:
  version: 2
  renderer: networkd
  ethernets:
    ${NETWORK_DEVICE}:
      addresses: [ ${IP} ]
      routes:
      - to: default 
        via: ${GATEWAY} 
        metric: 100
        on-link: true

      nameservers:
          search: [ ${DOMAIN} ]
          addresses: [ ${DNS01} ]
EOF
	else
cat >/etc/netplan/01-netcfg.yaml<<EOF
## 1ST RUN WIZZARD
network:
  version: 2
  renderer: networkd
  ethernets:
    ${NETWORK_DEVICE}:
      addresses: [ ${IP} ]
      routes:
      - to: default 
        via: ${GATEWAY}
        metric: 100
        on-link: true

      nameservers:
          search: [ ${DOMAIN} ]
          addresses: [ ${DNS01}, ${DNS02} ]
EOF
fi

}

#########################################
## SET NEW NETWORK CONFIG
#########################################
function set_networking  () {

	if [ -f /etc/network/interfaces ]; then
		msg_ok "BACKING UP INTERFACE CONFIG" "/etc/network/interfaces ==> /etc/network/interfaces_UNUSED"
		if [ ! -f /etc/network/interfaces_UNUSED ]; then
			mv /etc/network/interfaces /etc/network/interfaces_UNUSED
		fi
	fi

	if [ -f /etc/netplan/01-netcfg.yaml ]; then
		msg_ok "BACKING UP INTERFACE CONFIG" "/etc/netplan/01-netcfg.yaml ==> /etc/netplan/01-netcfg.yaml_UNUSED"
		if [ ! -f /etc/netplan/01-netcfg.yaml_UNUSED ]; then
			mv /etc/netplan/01-netcfg.yaml /etc/netplan/01-netcfg.yaml_UNUSED
		fi
	fi
	if [ -f /etc/netplan/00-installer-config.yaml ]; then
		msg_ok "BACKING UP INTERFACE CONFIG" "/etc/netplan/00-installer-config.yaml ==> /etc/netplan/00-installer-config.yaml_UNUSED"
		if [ ! -f /etc/netplan/00-installer-config.yaml_UNUSED ]; then
			mv /etc/netplan/00-installer-config.yaml /etc/netplan/00-installer-config.yaml_UNUSED
		fi
	fi

	#########################################
	## IF NO SECUNDARY DNS SERVER
	#########################################
	if [ ! "${DNS02}" ]; then
		## ETHERNET
		create_netplan_cfg
		if [ $? -eq 0 ]; then
			set_hostname
			set_search_domain
		else
			msg_error "CONFIG ERRORS" "ABORTING"
			sleep 3
			$0
		fi

	#########################################
	## IF SECUNDARY DNS SERVER AVAILABLE
	#########################################
	else
		## ETHERNET
		create_netplan_cfg
		if [ $? -eq 0 ]; then
			set_hostname
			set_search_domain
		else
			msg_error "CONFIG ERRORS" "ABORTING"
			sleep 3
			$0
		fi

		if [ ! -L /etc/resolv.conf ]; then
			rm /etc/resolv.conf 2>&1>/dev/null
		elif [ -f /etc/resolv.conf ]; then
			rm /etc/resolv.conf 2>&1>/dev/null
		fi

		ln -s /lib/systemd/resolv.conf /etc/resolv.conf
	fi
}

#########################################
## RESTART NETWORKING
#########################################
function network_restart () {
	netplan apply
}


#########################################
## SET HOSTNAME NEW + LEGACY PROCESS 
#########################################
function set_hostname () {
	hostnamectl set-hostname ${HOSTN}.${DOMAIN} &
	echo "${HOSTN}.${DOMAIN}" > /etc/hostname
	export HOSTNAME=${HOSTN}.${DOMAIN}

	## IP=10.20.139.205/24 ==> HOST_IP=IP=10.20.139.205 
	HOST_IP=${IP%/*}
	egrep "${HOST_IP}|${HOSTN}" /etc/hosts &>/dev/null
	if [ $? -ne 0 ]; then
		echo "## $( date -I ) ADDED BY [ $0 ]"        >> /etc/hosts
		echo "${HOST_IP} ${HOSTN}.${DOMAIN} ${HOSTN}" >> /etc/hosts
	fi

	egrep "${DOCKER_REG_IP}|${DOCKER_REG_HOST}" /etc/hosts &>/dev/null
	if [ $? -ne 0 ]; then
		echo "${DOCKER_REG_IP} ${DOCKER_REG_HOST}"    >> /etc/hosts
	fi

}

#########################################
## SET SEARCH DOMAIN 
#########################################
function set_search_domain () {
	if [ -f /etc/resolvconf/resolv.conf.d/head ]; then
		grep "${DOMAIN}" /etc/resolvconf/resolv.conf.d/head &> /dev/null
		if [ $? -ne 0 ]; then
			echo "search ${DOMAIN}" >> /etc/resolvconf/resolv.conf.d/head
		fi
	fi

	if [ -f /etc/systemd/resolved.conf ]; then
cat >/etc/systemd/resolved.conf<<EOF
[Resolve]
#DNS=
#FallbackDNS=
Domains=${DOMAIN}
#LLMNR=no
#MulticastDNS=no
#DNSSEC=no
#Cache=yes
#DNSStubListener=yes
EOF
	fi
	## RESTART RESOLVER
	## systemctl restart systemd-resolved
}

#########################################
## VALIDATE NETWORK CONFIG
#########################################
function config_network () {

    	echo -e "${RED}---------------------------------------${NORMAL}"
        echo "CONFIGURE YOUR NEW INTERNET CONNECTION"
    	echo -e "${RED}---------------------------------------${NORMAL}"

	echo -ne "HOSTNAME ${YELLOW} ${HOST_NAME%%.*}${WHITE} : "
        read HOSTN
        [[ ! "${HOSTN}" ]] && HOSTN="${HOST_NAME%%.*}"

	echo -ne "DOMAIN ${YELLOW} ${DOMAIN_NAME}${WHITE} : "
        read DOMAIN
        [[ ! "${DOMAIN}" ]] && DOMAIN="${DOMAIN_NAME}"

	echo -ne "IP       ${YELLOW} ${HOST_IP} ${WHITE}: "
        read IP
        [[ ! "${IP}" ]] && IP="${HOST_IP}"

	echo -ne "GATEWAY  ${YELLOW} ${GATE_WAY}${WHITE} : "
        read GATEWAY
        [[ ! "${GATEWAY}" ]] && GATEWAY="${GATE_WAY}"

	echo -ne "DNS01    ${YELLOW} ${DNS_01}${WHITE} : "
        read DNS01
        [[ ! "${DNS01}" ]] && DNS01="${DNS_01}"

	echo -ne "DNS02    ${YELLOW} ${DNS_02}${WHITE} : "
        read DNS02
        [[ ! "${DNS02}" ]] && DNS02="${DNS_02}"
	
	show_network
}

#########################################
## CHECK NETWORK CONFIG
#########################################
function show_network () {

        echo -e "${RED}---------------------------------------${WHITE}"
        echo -e "HOSTNAME:${GREY} ${HOSTN} ${WHITE}"
        echo -e "DOMAIN:  ${GREY} ${DOMAIN} ${WHITE}"
        echo -e "IP:      ${GREY} ${IP} ${WHITE}"
        echo -e "GATEWAY: ${GREY} ${GATEWAY} ${WHITE}"
        echo -e "DNS01:   ${GREY} ${DNS01} ${WHITE}"
        echo -e "DNS02:   ${GREY} ${DNS02} ${WHITE}"
    	echo -e "${RED}---------------------------------------${NORMAL}"

    	unset INPUT
        while [ ! "${INPUT}" = "YES" ]; do

                msg_ask "IS EVERYTHING CORRECT?" "YES | NO"
        	read INPUT
        	if [ "${INPUT}" = "YES" ] || [ "${INPUT}" = "yes" ]; then
                	unset INPUT
			set_networking
			## network_restart #&
                	break
		elif [ "${INPUT}" = "NO" ] || [ "${INPUT}" = "no" ]; then
                        config_network 
                fi
        done
}

#########################################
## SYSTEM CLEANUP
#########################################
function cleanup () {
	msg_ok "CLEANING UP SYSTEM" "LOGFILES"
	find /var/ -type f -name *log -exec rm {} \;

	rm /var/log/dmesg* 	 2>/dev/null
	rm /var/log/kern* 	 2>/dev/null
	rm /var/log/syslog* 	 2>/dev/null
	rm /var/log/auth* 	 2>/dev/null
	rm /var/log/wtmp* 	 2>/dev/null
	rm /var/tmp/traefik* -rf 2>/dev/null

	systemctl restart rsyslog

	msg_ok "CLEANING UP SYSTEM" "APT"
	apt-get autoremove -y
	dpkg --configure -a
	apt-get install -f
	apt-get autoremove -y
	apt-get clean

	/usr/bin/dpkg -l | grep "^rc" &>/dev/null
	if [ $? -eq 0 ]; then
        	/usr/bin/dpkg --purge $( /usr/bin/dpkg -l | grep "^rc" | /usr/bin/awk '{ print $2 }' )
	fi

	## REMOVING SNAP SNAPSHOTS
	for SNAP in $( snap saved | awk '{ print $1 }' | grep -v Set )
	do 
		snap forget ${SNAP}
	done

}
