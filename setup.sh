#!/bin/bash
########################################################################################
##
##      WRITTEN FOR UBUNTU
##
##      VERSION:        0.0.1
##      DATE:           01.03.2004
##      MODIFIED:       29.01.2025
##      MAINTAINER:     mad.murdock 
##      LICENZE:        >=GPLv2
##
########################################################################################
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
MY_DIR="/opt/scripts/microk8s"

########################################################
## INCLUDE FILES
########################################################
. ${MY_DIR}/config/setup.cfg
. ${MY_DIR}/include/ansicolors.inc.sh
. ${MY_DIR}/include/setup_functions.inc.sh
. ${MY_DIR}/include/logo_functions.inc.sh
. ${MY_DIR}/include/msg_functions.inc.sh
. ${MY_DIR}/include/microk8s_functions.inc.sh
. ${MY_DIR}/include/get_network_info_functions.inc.sh

###############################
## MAIN PROCEDURES 
###############################

############################################################################
## MENUE ENTRY // MAKE YOUR CHOICE
############################################################################
function menue () {
        while [ ! "${CHOICE}" ]; do
		clear
		logo
        	echo -e "${RED}--------------------------------------------------------------${NORMAL}"
                echo -e " [ ${RED}0${NORMAL} ] :: SETUP HOST"
                echo -e " [ ${RED}1${NORMAL} ] :: SETUP MICROK8S"
                echo -e " [ ${RED}3${NORMAL} ] :: CLEANUP SYSTEM"
                ## echo -e " [ ${RED}2${NORMAL} ] :: RESTART PUPPET"
        	echo -e "${RED} ---------------------${NORMAL}"
                echo -e " [ ${RED}10${NORMAL} ] :: POWEROFF"
                echo -e " [ ${RED}11${NORMAL} ] :: REBOOT"
        	echo -e "${RED}--------------------------------------------------------------${NORMAL}"
        	echo
        	echo -ne "${NORMAL}YOUR CHOICE: "
                read CHOICE
        done

}

menue

## CONFIGURE HOST 
if [ "${CHOICE}" = "0" ]; then
	get_network_info
	host_config
	config_network

	unset CHOICE
	cd ${MY_DIR}
	read -p "PRESS ENTER TO PROCEEDE"
	$0

## INIT MICROK8S
elif [ "${CHOICE}" = "1" ]; then
	install_microk8s
	install_traefik
	unset CHOICE
	msg_ask "IN ORDER TO PROCCEDE PRESS" "ENTER"
	read
	cd ${MY_DIR}
	$0

## CLEANUP SYSTEM
elif [ "${CHOICE}" = "3" ]; then
	cleanup
	unset CHOICE
	msg_ask "IN ORDER TO PROCCEDE PRESS" "ENTER"
	read
	cd ${MY_DIR}
	$0

## POWEROFF
elif [ "${CHOICE}" = "10" ]; then
	unset CHOICE

	msg_ask "POWEROFF" "YES | NO"
        read INPUT

        if [  "${INPUT}" = "YES" ] || [ "${INPUT}" = "yes" ]; then
		msg_error "SHUTTING DOWN" "..."
		poweroff
	else
		cd ${MY_DIR}
		$0
        fi

## REBOOT
elif [ "${CHOICE}" = "11" ]; then
	unset CHOICE

	msg_ask "REBOOT" "YES | NO"
        read INPUT

	if [  "${INPUT}" = "YES" ] || [ "${INPUT}" = "yes" ]; then
		msg_error "REBOOTING" "..."
		reboot
	else
		$0
        fi

else
	unset CHOICE
	$0

fi

