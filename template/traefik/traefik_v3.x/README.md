## TREAFIK
```
https://doc.traefik.io/traefik/master/reference/dynamic-configuration/kubernetes-crd/#resources
https://doc.traefik.io/traefik/user-guides/crd-acme/#traefik-routers
```

## INGRESS ROUTE TLS SECRET 
- Change wc-example-domain to a name of your choice
- Change /path/to/your/cert/**wc.example.domain.cert** to your tls certificate
- Change /path/to/your/cert/**wc.example.domain.key** to your tls certificates key
```
kubectl create secret tls wc-example-domain --save-config --dry-run=client --cert=/path/to/your/cert/wc.example.domain.crt --key /path/to/your/cert/wc.example.domain.key -n kube-system -o yaml | kubectl apply -f -
```

### MODFIY treafik-ingress-route.yaml.example 
- Change **traefik.example.domain** to your real traefik domain
- Change **wc-example-domain** to ^^^ before created secret name 
- Rename **treafik-ingress-route.yaml.example** to **treafik-ingress-route.yaml**

### APPLY INGRESS
```
kubectl apply -f treafik-ingress-route.yaml
```
